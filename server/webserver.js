import express from 'express'; 
import dotenv from 'dotenv'
import { fileURLToPath } from 'url';
import { dirname } from 'path';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

dotenv.config({ path: '../config/.env' })

let server = express();
let port = process.env.PORT

server.use('/', express.static(__dirname+"/../public"));
console.log("Listen "+port)
server.listen(port);

export default ()=>{}
