// import tw, { test } from "./tw.js";
import webserver from "./webserver.js";
import webwebsocketserver from "./websocketserver.mjs";
// import isolate from "./isolate.js";
import fetch from 'node-fetch';

//tw.test();
const shares = webwebsocketserver();
const client_id = 's4o0lhodcq769no4a0cn6llagtrfxw'

import fs from 'fs';

import { dirname } from 'path';
import { fileURLToPath } from 'url';
const __dirname = dirname(fileURLToPath(import.meta.url));

console.log(__dirname)

// File destination.txt will be created or overwritten by default.
fs.copyFile(__dirname + '/shared.js', __dirname + '/../public/shared.js', (err) => {
  if (err) throw err;
  console.log('shared.js was copied to public');
});

shares.subscribe(({share,prop,value}) => {

  if (prop == "access_token") {
    fetch('https://api.twitch.tv/helix/users',
      {
        method: 'get',
        headers: {
          'Client-ID': client_id,
          'Authorization': 'Bearer ' + value
        }
      })
      .then(res => res.json())
      .then(json => {
        console.log(json)
        share.proxy.logged = true //LOGGED IN
      })
      .catch(err => console.log(err));

  }

  // console.log(new Date(),share_event.prop,share_event.value)
  //let share = share_event.share
  //share.proxy.pong = 2
})


setInterval(shares.broadcast, 10000, "alive", true)
setInterval(shares.broadcast, 9000, "alive", "true")
setInterval(shares.broadcast, 8000, "alive", 10)
setInterval(shares.broadcast, 7000, "alive", 10.1)

//setInterval( ()=>{share.i=1} , 1000 )