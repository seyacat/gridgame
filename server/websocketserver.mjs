#!/usr/bin/env node
//import WebSocket from 'websocket';
import WebSocket, { WebSocketServer } from 'ws';
import  https from 'https' 
import http from 'http';
import dotenv from 'dotenv';
import Share, {Shares} from './shared.js'
import { fileURLToPath } from 'url';
import { dirname } from 'path';
import path from 'path';
import fs from 'fs';
import { json } from 'express';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const serverOptions = {
  host: '0.0.0.0',
  port: process.env.WS_PORT,
  routes: {
    cors: {
      origin: ['*']
    }
  }
};
//CERTS
const serverPathRoot = path.resolve(__dirname,  'certs');
console.log(serverPathRoot + '/cert.pem',fs.existsSync(serverPathRoot + '/cert.pem'));
if (fs.existsSync('/etc/letsencrypt/live/backend.seyacat.com/fullchain.pem') && fs.existsSync('/etc/letsencrypt/live/backend.seyacat.com/privkey.pem')) {
  console.log("enable backend.seyacat.com tls")
  serverOptions.tls = {
    // If you need a certificate, execute "npm run cert".
    cert: fs.readFileSync('/etc/letsencrypt/live/backend.seyacat.com/fullchain.pem'),
    key: fs.readFileSync('/etc/letsencrypt/live/backend.seyacat.com/privkey.pem')
  };
}
else if (fs.existsSync(serverPathRoot + '/cert.pem') && fs.existsSync(serverPathRoot + '/privkey.pem')) {
  console.log("enable tls")
  serverOptions.tls = {
    // If you need a certificate, execute "npm run cert".
    cert: fs.readFileSync(serverPathRoot + '/cert.pem'),
    key: fs.readFileSync(serverPathRoot + '/privkey.pem')
    //cert: fs.readFileSync('/etc/letsencrypt/live/backend.seyacat.com/fullchain.pem'),
    //key: fs.readFileSync('/etc/letsencrypt/live/backend.seyacat.com/privkey.pem')
  };
}


const shares = new Shares()
let shareConfig
export default  (config)=>{
  shareConfig = config
  return shares}

dotenv.config({ path: '../config/.env' })

var httpsServer = https.createServer(serverOptions.tls);
var wss = new WebSocketServer({
    server: httpsServer
});

wss.on('connection', function connection(ws) {
    const share = new Share({...shareConfig,connection:ws})
    shares.pushShare(share)
    console.log(ws.conn_id + ' connected');
    ws.on('message', function incoming(message) {
          try {
            var jsonmsg = JSON.parse(message);
          } catch (e) {
            var jsonmsg = null;
          }
        if(jsonmsg){
          share.remoteSetFunction( jsonmsg.prop,jsonmsg.value )
        }
    });

    ws.on('close', function (code, reason) {
          console.log(ws.conn_id + ' disconnected');
    });

    ws.send("send channel id in json {channel_id:xxxxxx}");
});

//websocket de clientes
console.log("listen wss "+process.env.WS_PORT)
httpsServer.listen(process.env.WS_PORT); 

