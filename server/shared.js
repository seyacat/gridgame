export class Shares {
  constructor() {
    this.subscribers = []
    this.shares = []
  }
  subscribe = function (subs) {
    this.subscribers.push(subs)
  }
  setCallback = function () {
    this.subscribers.forEach(cb => { cb(...arguments) })
  }
  broadcast = function (prop, value) {
    for (let share of this.shares) { share.proxy[prop] = value 
      console.log(prop)
      //console.log(share)
    }
  }.bind(this)
  pushShare(newshare) {
    this.shares.push(newshare)
    newshare.subscribe(this.setCallback.bind(this))
  }
}

export class Share {
  subscribe = function (callback) {
    this.subscribers.push({ callback: callback })
  }
  remoteSetFunction = function (key, value) {
    if (this.protected.includes(key) || this.private.includes(key)) {
      return false
    }
    this.data[key] = value
    for (let sub of this.subscribers) {
      //sub.callback.bind(sub.target)(sub.share, key, value)
      sub.callback({ share: this, prop: key, value: value })
    }
    return true
  }
  setFunction = function (target, prop, value) {
    if (typeof window === 'undefined') {//NODE
      if (!this.protected.includes(prop) && !this.private.includes(prop) && this.connection) {
        console.log("send",{ prop: prop, value: value })
        this.connection.send(JSON.stringify({ prop: prop, value: value }))
      }
    } else {
      if (!this.private.includes(prop) && this.connection && this.connection.readyState === 1) {
        this.connection.send(JSON.stringify({ prop: prop, value: value }))
      }
    }
    target[prop] = value
    return true
  }
  constructor(config) {
    this.config = config
    this.data = { share: this }
    this.subscribers = []
    this.protected = ["connection"]
    this.private = []
    this.readonly = []
    if (config && typeof (config) === "object") {
      Object.entries(config).forEach((item) => { this[item[0]] = item[1] })
    }
    this.proxy = new Proxy(this.data, {
      set: function (target, prop, value) {
        if (prop === "share") {
          return false
        }
        return target.share.setFunction(target, prop, value)
      }
    }
    )
  }
}
export default Share

export class ShareBrowser extends Share {

  connect = function (share, url) {
    var ws = new WebSocket(url);
    //var ws = new WebSocket('wss://backend.seyacat.com:4445');
    ws.onopen = function () {
      share.connection = ws
      share.proxy.access_token = share.access_token;
    };

    ws.onmessage = function (event) {
      let json_message = null;
      try {
        json_message = JSON.parse(event.data);
      } catch (e) { }
      if (json_message) {
        share.remoteSetFunction(json_message.prop, json_message.value)
      }
    };

    ws.onclose = function (e) {
      console.log('Socket is closed. Reconnect will be attempted in 1 second.', e.reason);
      setTimeout(function () {
        this.connect(this, this.config.url)
      }.bind(this), 3000);
    }.bind(this);

    ws.onerror = function (err) {
      console.error('Socket encountered error: ', err.message, 'Closing socket');
      ws.close();
      share.login = ""
    };
  }

  constructor(config) {
    super(config)
    //super.constructor(config)
    this.connect(this, this.config.url)
  }
}